using System;
class Vigenere: Crypt {
    string m_key;
     public Vigenere(string _key) {
        m_key = "";
        foreach (char __k in _key)
            if (char.IsLetter(__k))
                m_key += __k;
    }
    override public string Cipher(string _plaintext, bool _encipher) {
        string _ciphertext = "";
        int _nonAlphaCharCount = 0;
        for (int i = 0; i < _plaintext.Length; i++) {
            if (char.IsLetter(_plaintext[i])) {
                bool __isUpper = char.IsUpper(_plaintext[i]);
                char __offset = __isUpper ? 'A' : 'a';

                int __kIndex = (i - _nonAlphaCharCount) % m_key.Length;
                int k = (__isUpper ? char.ToUpper(m_key[__kIndex]) : char.ToLower(m_key[__kIndex])) - __offset;
                k = _encipher ? k : -k;

                char c = (char) (ModPositive(_plaintext[i] + k - __offset, 26) + __offset);
                _ciphertext += c;
            } else {
                _ciphertext += _plaintext[i];
                _nonAlphaCharCount++;
            }
        }
        return _ciphertext;
    }
}