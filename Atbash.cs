using System;
class Atbash: Crypt {
    override public string Cipher(string _plaintext, bool _encipher) {
        string _ciphertext = "";

        for (int i = 0; i < _plaintext.Length; i++) {
            if (char.IsLetter(_plaintext[i])) {
                bool __isUpper = char.IsUpper(_plaintext[i]);
                char __offset = __isUpper ? 'A' : 'a';

                char c = (char) (ModPositive(25 - (_plaintext[i]-__offset), 26) + __offset);
                _ciphertext += c;
            } else {
                _ciphertext += _plaintext[i];
            }
        }

        return _ciphertext;
    }
}