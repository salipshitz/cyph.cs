abstract class Crypt {
    public int ModPositive(int a, int b) {
        return (a % b + b) % b;
    }
    public abstract string Cipher(string _plaintext, bool _encipher);
}