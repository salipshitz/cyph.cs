﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace Coding {
class Program {
    private static string filePath = Directory.GetCurrentDirectory()+"/file.dat";
    private static Dictionary<string, Crypt> crypts;
    public static void Main(string[] _args) {
        crypts = new Dictionary<string, Crypt>();
        Console.Clear();
        Console.WriteLine("Cyph.CS encryptor/decryptor v1.0");
        string result = "";
        for(;;) {
            Console.Write("$ ");
            string cmd = Console.ReadLine();
            string[] os = cmd.Split(" ");
            if (cmd == "help") {
                Console.WriteLine("Command list:");
                Console.WriteLine("help: Access command list");
                Console.WriteLine("new [cipher_type] [name] <withkey [key]>: Create new cipher key. Don't add key for ciphers that don't have keys (e.g. ROT-13, Atbesh)");
                Console.WriteLine("encrypt [plaintext] withcipher [name]: Encrypts plaintext with cipher with specified name");
                Console.WriteLine("decrypt [plaintext] withcipher [name]: Decrypts ciphertext with cipher with specified name");
                Console.WriteLine("list ciphers: list of all ciphers");
                Console.WriteLine("clear: clear Cyph.CS window");
            } else if (os[0] == "new") {
                if (os[1].ToLower() == "vigenere") {
                    if (os[3] == "withkey") {
                        crypts.Add(os[2], new Vigenere(os[4]));
                    } else
                        Console.WriteLine("Error: Invalid format. 'new' function must be in the format: new [cipher_type] called [name] withkey [key]");
                } else if (os[1].ToLower() == "caesar") {
                    if (os[3] == "withkey" && os.Length == 5) {
                        crypts.Add(os[2], new Caesar(os[4]));
                    } else
                        Console.WriteLine("Error: Invalid format. 'new' function must be in the format: new [cipher_type] called [name] withkey [key]");
                } else if (os[1].ToLower() == "rot13") {
                    crypts.Add(os[2], new Caesar("13"));
                } else if (os[1].ToLower() == "atbash") {
                    crypts.Add(os[2], new Atbash());
                } else
                        Console.WriteLine("Error: unknown cipher. Type 'list ciphers' for a list of all the ciphers");
            } else if (os[0] == "encrypt") {
                if (os[os.Length-2] == "withcipher") {
                    if (os[1] == "lastresult")
                        result = crypts[os[os.Length-1]].Cipher(result, true);
                    else
                        result = crypts[os[os.Length-1]].Cipher(string.Join(" ", os, 1, os.Length-3), true);
                    Console.WriteLine(result);
                } else
                    Console.WriteLine("Error: Invalid format. 'encrypt' function must be in the format: encrypt [plaintext] withcipher [name]");
            } else if (os[0] == "decrypt") {
                if (os[2] == "withcipher") {
                    if (os[1] == "lastresult")
                        result = crypts[os[os.Length-1]].Cipher(result, false);
                    else
                        result = crypts[os[os.Length-1]].Cipher(string.Join(" ", os, 1, os.Length-3), false);
                    Console.WriteLine(result);
                } else
                    Console.WriteLine("Error: Invalid format. 'decrypt' function must be in the format: decrypt [plaintext] withcipher [name]");
            } else if (cmd == "list ciphers") {
                Console.WriteLine("Vigenere (string key): Cycles through key, adds the current letter in the key with the current letter in the plaintext.");
                Console.WriteLine("Caesar (int key): Adds the key to the current letter in the plaintext.");
                Console.WriteLine("ROT13 (no key): Caesar of 13, encryption is the same as decryption");
                Console.WriteLine("Atbash (no key): Reverses each character in plaintext (e.g. A->Z, B->Y)");
            } else if (cmd == "exit")
                Environment.Exit(0);
            /*else if (cmd == "save") {
                string[] _save = new string[crypts.Count+1];
                _save[0] = result;
                for (int i = 0; i < crypts.Count; i++) {
                    string key = crypts.ElementAt(i).Key;
                    Crypt val = crypts.ElementAt(i).Value;
                    string s = key+"/"+val.GetType().ToString();
                }
            } else if (cmd == "load") {

            }*/ else if (cmd == "clear") {
                Console.Clear();
                Console.WriteLine("Cyph.CS encryptor/decryptor v1.0");
            } else
                Console.WriteLine("Error: Command unknown. Type help for list of commands.");
            }
        }
    }
}
