using System;
class Caesar: Crypt {
    int m_key;
    public Caesar(string _key) {
        m_key = Convert.ToInt16(_key);
    }
    override public string Cipher(string _plaintext, bool _encipher) {
        string _ciphertext = "";

        for (int i = 0; i < _plaintext.Length; i++) {
            if (char.IsLetter(_plaintext[i])) {
                bool __isUpper = char.IsUpper(_plaintext[i]);
                char __offset = __isUpper ? 'A' : 'a';
                
                int k = _encipher ? m_key : -m_key;

                char c = (char) (ModPositive(_plaintext[i] + k - __offset, 26) + __offset);
                _ciphertext += c;
            } else {
                _ciphertext += _plaintext[i];
            }
        }

        return _ciphertext;
    }
}